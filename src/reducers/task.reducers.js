import { TASK_QUANTITY_HANDLER } from "../constants/task.constant";

const initialState = {
  mobileList : [
    {
      name: "IPhone X",
      price: 900,
      quantity: 0
    },
    {
      name: "Samsung S9",
      price: 800,
      quantity: 0
    },
    {
      name: "Nokia 8",
      price: 650,
      quantity: 0
    }
  ]
}
const taskReducer =(state = initialState, action) =>{
    switch(action.type){
        case TASK_QUANTITY_HANDLER:
            state.mobileList[action.payload].quantity++;
            break;
    }
    return {...state}
}
export default taskReducer;