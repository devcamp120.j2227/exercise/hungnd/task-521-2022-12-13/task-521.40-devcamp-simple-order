import { Button, Container, Grid } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { quantityHandle } from "../actions/task.action";
const Task = () =>{
    // B3: Khai báo dispatch để đẩy action tới reducer
    const dispatch = useDispatch();
    //B1: nhận giá trị khởi tạo của state trong giai đoạn mở đầu
    const {mobileList} = useSelector((reduxData)=>{
        return reduxData.taskReducer;
    });
    //tính tổng giá tiền đơn hàng dựa trên số lượng hàng đã chọn
    const total = mobileList.reduce((summary, item) => summary+ item.price*item.quantity, 0);
    //B2:
    const onButtonBuyClickHandler = (id) =>{
        dispatch(quantityHandle(id));
    }
    return (
        <Container>
            <Grid container spacing={5} mt={5} style={{padding:"0px"}}>
                {mobileList.map((value,index) =>{
                    return (
                    < Grid item xs={4} key={index}>
                        <Grid container rowSpacing={1} sx={{
                        width: 320,
                        height: 300,
                        border:"1px solid"}} style={{display:"flex",alignItems: "center"}}>
                            <Grid style={{marginLeft:"50px", marginBottom:"10px"}}>
                                <h4>{value.name}</h4>
                                <p>{value.price}</p>
                                <p >Quantity:{value.quantity}</p>
                                <Button style={{backgroundColor:"green", color:"white"}} onClick={()=>onButtonBuyClickHandler(index)}>Buy</Button>
                            </Grid>
                        </Grid>
                    </Grid>
                    )   
                })}
            </Grid>
            <Grid>
                <h4>Total: {total} USD</h4>
            </Grid>
        </Container>
        
    )
}
export default Task;